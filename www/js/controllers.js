angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};
  // ページ下部の infinite-scroll を表示するか否かのフラグ
//  $scope.initial_loaded = false;

  // Loading... を表示
//  $ionicLoading.show({
//    template: 'Loading...',
//    noBackdrop: true
//  });

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Loading... を隠す
//  $ionicLoading.hide();
//  // ページ上部のリフレッシュ表示を終了
//  $scope.$broadcast('scroll.refreshComplete');

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('TasklistsCtrl', function($scope) {
  $scope.Tasklists = [
    { title: '製造番号-001', id: 1 },
    { title: '製造番号-002', id: 2 },
    { title: '製造番号-003', id: 3 },
    { title: '製造番号-004', id: 4 },
    { title: '製造番号-005', id: 5 },
    { title: '製造番号-006', id: 6 }
  ];
})

.controller('LinerHoleCtrl', function($scope) {
  $scope.LinerHolelists = [
    { title: 'A1/2',Pich: 0.0 ,Px: 1, id: 1 },
    { title: 'A1/2',Pich: 0.0 ,Px: 2, id: 2 },
    { title: 'A2/2',Pich: 0.0 ,Px: 2, id: 3 },
    { title: 'A2/2',Pich: 0.0 ,Px: 3, id: 4 },
    { title: 'A3/2',Pich: 0.0 ,Px: 3, id: 5 },
    { title: 'A3/2',Pich: 0.0 ,Px: 4, id: 6 },
    { title: 'A4/2',Pich: 0.0 ,Px: 4, id: 7 },
    { title: 'A4/2',Pich: 0.0 ,Px: 5, id: 8 },
    { title: 'A5/2',Pich: 0.0 ,Px: 5, id: 9 },
    { title: 'A5/2',Pich: 0.0 ,Px: 6, id: 10 },
    { title: 'A6/2',Pich: 0.0 ,Px: 6, id: 11 },
    { title: 'A6/2',Pich: 0.0 ,Px: 7, id: 12 },
    { title: 'A7/2',Pich: 0.0 ,Px: 7, id: 13 },
    { title: 'A7/2',Pich: 0.0 ,Px: 8, id: 14 },
    { title: 'A8/2',Pich: 0.0 ,Px: 8, id: 15 },
    { title: 'A8/2',Pich: 0.0 ,Px: 9, id: 16 },
    { title: 'A9/2',Pich: 0.0 ,Px: 9, id: 17 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
